﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CalculatorApplication.Models;

namespace CalculatorApplication.Controllers
{
    public class CalcController : Controller
    {
        // GET: Calc
        [HttpGet]
        public ActionResult Index()
        {
            return View(new calc());
        }

        [HttpPost]
        public ActionResult Index(calc obj, string calculate)
        {
            if(calculate == "add")
            {
                obj.result = obj.number_first + obj.number_second;
            }
            else if (calculate == "sub")
            {
                obj.result = obj.number_first - obj.number_second;
            }
            else if (calculate == "mul")
            {
                obj.result = obj.number_first * obj.number_second;
            }
            else if (calculate == "div")
            {
                obj.result = obj.number_first / obj.number_second;
            }
            else
            {
                obj.result = obj.number_first % obj.number_second;
            }
            return View(obj);
        }
    }
}